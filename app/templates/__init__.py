table_template = """
<table width="80%%">
    <tr>
        <th class="datecol">Start date</th>
        <th class="intcolumn">Period lapse (days)</th>
        <th class="intcolumn">Duration (days)</th>
        <th>hello</th>
    </tr>
    %s
</table>
"""

row_template = """
<tr>
    <td class="datecol">%s</td>
    <td class="intcolumn">%s</td>
    <td class="intcolumn">%s</td>
    <td>delete</td>
</tr>
"""
