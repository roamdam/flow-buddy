
from psycopg2 import connect


def postgres_connect():
    return connect(
        user = "postgres",
        password = "",
        host = "127.0.0.1",
        port = "5432",
        database = "flowbuddy"
    )
