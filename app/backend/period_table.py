from datetime import date

from app.backend import postgres_connect


class PeriodTable:
    name = "flows"
    
    def __init__(self):
        self.rows = PeriodTable.get_table()
    
    @staticmethod
    def get_table() -> list:
        """Get dates from flows table."""
        conn = postgres_connect()
        cursor = conn.cursor()
        cursor.execute("""
            SELECT start_date, end_date FROM %s
            ORDER BY start_date DESC
        """ % PeriodTable.name)
        rows = cursor.fetchall()
        conn.close()
        return rows
    
    @staticmethod
    def format_date(x: date, target_format: str) -> str:
        """Format a date format to string."""
        try:
            res = x.strftime(target_format)
        except AttributeError:
            raise AttributeError("Input object %s is not in datetime format." % x)
        return res
    
    @staticmethod
    def duration(x: date, y: date) -> int:
        """Compute difference between to datetimes, in days."""
        assert isinstance(x, date), "x must be datetime, was %s (%s)" % (x, type(x))
        assert isinstance(y, date), "y must be datetime, was %s (%s)" % (y, type(y))
        dur = x - y
        return abs(dur.days)
    
    def attach_previous(self):
        """Convert flows list of tuples into list of dictionaries.
        
        A field ``previous`` is added during item conversion to dictionary,
        containing the same index value from the next row (which is the previous
        period).
        """
        rows = self.rows
        if len(rows) > 1:
            new_rows = [
                {
                    "start": rows[row_number][0],
                    "end": rows[row_number][1],
                    "previous": rows[row_number + 1][0]
                } for row_number in range(len(rows) - 1)
            ]
            new_rows.append({
                "start": rows[len(rows)-1][0],
                "end": rows[len(rows)-1][1],
                "previous": None
            })
        elif len(rows) == 1:
            new_rows = [{
                "start": rows[0][0],
                "end": rows[0][1],
                "previous": None
            }]
        else:
            new_rows = []
        self.rows = new_rows
        return self
    
    def build_table(self, r_template: str, t_template: str, limit: int) -> str:
        """Build an html string table from a row and a table template."""
        shrug = '¯\\_(ツ)_/¯'
        rows = []
        for item in self.rows[:limit]:
            assert isinstance(item, dict), "self.rows must have been converted to dictionaries."
            try:
                duration = PeriodTable.duration(x = item['start'], y = item["end"])
            except AssertionError:
                duration = shrug
            try:
                lapse = PeriodTable.duration(x = item['start'], y = item["previous"])
            except AssertionError:
                lapse = shrug
            row = r_template % (
                PeriodTable.format_date(x = item['start'], target_format = "%d %B (%A)"),
                lapse,
                duration
            )
            rows.append(row)
        table = t_template % ''.join(rows)
        return table

    def period_starts(self):
        return [record["start"] for record in self.rows]
