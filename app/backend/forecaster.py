
from datetime import date
from datetime import timedelta
from statistics import median
from re import sub


class Forecaster:
    
    def __init__(self, records: list):
        self.records = Forecaster._validate_records(records = records)
        self.durations = None
        self.forecast = None
        
    @staticmethod
    def _validate_records(records: list) -> list:
        """Validate input records : must be dates. Then sort them in descending order."""
        for index, record in enumerate(records):
            if not isinstance(record, date):
                raise TypeError("record %s (index %s) is not a date" % (record, index))
        return sorted(records, reverse = True)
        
    def build_durations(self):
        """Build n-1 durations from n date records (in days)."""
        records = self.records
        self.durations = [(records[index] - records[index + 1]).days for index in range(len(records) - 1)]
        return self
        
    def forecast_date(self):
        """Forecast the next period date from median period duration."""
        if len(self.durations) == 0:
            forecast = date(1970, 1, 1)
        else:
            median_dur = int(median(self.durations))
            forecast = self.records[0] + timedelta(median_dur)
        self.forecast = forecast
        return self
        
    def today_distance(self) -> int:
        """Distance between forecast and today, in days."""
        today_date = date.today()
        distance = self.forecast - today_date
        return distance.days
    
    @staticmethod
    def format_forecast(x: date) -> str:
        init_date = x.strftime("%A %d %B")
        whole_str = init_date.split(" ")
        day_str = whole_str[1]
        day_str = day_str[1] if day_str[0] == '0' else day_str
        return ' '.join((whole_str[0], day_str, whole_str[2]))
