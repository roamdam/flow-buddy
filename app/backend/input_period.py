
from datetime import datetime
from psycopg2 import Error as PsycError
from app.backend import postgres_connect


class PeriodDateHandler:
    """A handler for input start and end period dates.
    
    Convert received dates as datetime after validating them.
    """
    def __init__(self):
        self.start = None
        self.end = None
        
    def set_dates(self, start_date: str, end_date: str, fmt: str):
        """Set input dates to datetime format as attributes.
        
        None tolerance for end date but not for start date.
        """
        start = PeriodDateHandler.to_date(start_date, fmt = fmt, tolerance = False)
        end = PeriodDateHandler.to_date(end_date, fmt = fmt, tolerance = True)
        if end is not None and end <= start:
            raise ValueError('End date must be after start date.')
        self.start = start
        self.end = end
        return self
        
    @staticmethod
    def to_date(x: str, fmt: str, tolerance: bool) -> datetime:
        """Convert a string to datetime format, with None tolerance if needed."""
        try:
            res = datetime.strptime(x, fmt)
        except ValueError as e:
            if tolerance:
                res = None
            else:
                raise e
        return res
    
    def write_row(self) -> None:
        """Write start and end date to ``flows`` table in postgres."""
        conn = postgres_connect()
        cursor = conn.cursor()
        try:
            cursor.execute("""INSERT INTO flows (start_date, end_date) VALUES (%s, %s)""", (self.start, self.end))
        except PsycError:
            conn.rollback()
        else:
            conn.commit()
        finally:
            conn.close()
        return
