-- Don't forget to lower those mails when writing them !
--CREATE TABLE users (
--    id                SERIAL      PRIMARY KEY,
--    name              VARCHAR(80) NOT NULL
--    email             VARCHAR(80) NOT NULL UNIQUE,
--    password_digest   VARCHAR(80) NOT NULL
--);
--CREATE TABLE alerts (
--    user              INTEGER     PRIMARY KEY REFERENCES users (id) NOT NULL,
--    alert             BOOLEAN     NOT NULL DEFAULT FALSE
--);
--ALTER TABLE alerts ADD CONSTRAINT unique_user_alert UNIQUE (user, alert);
CREATE TABLE flows (
    id                SERIAL      PRIMARY KEY,
--    user              INTEGER     REFERENCES users (id) NOT NULL,
    start_date        DATE        NOT NULL,
    end_date          DATE        NOT NULL
);
ALTER TABLE flows ADD CONSTRAINT unique_start UNIQUE (start_date);
--ALTER TABLE flows ADD CONSTRAINT unique_user_flow UNIQUE (user, start_date);
--CREATE TABLE forecasts (
--    user              INTEGER     PRIMARY KEY REFERENCES users (id) NOT NULL,
--    start_date        DATE        NOT NULL
--);
--ALTER TABLE forecasts ADD CONSTRAINT unique_user_forecast UNIQUE (user, start_date);
