
from flask import Flask
from flask import render_template
from flask import request

from app.backend.input_period import PeriodDateHandler
from app.backend.period_table import PeriodTable
from app.backend.forecaster import Forecaster

from app.templates import row_template
from app.templates import table_template

application = Flask(__name__)


@application.route('/', methods = ['GET'])
def home():
    nb_displayed_rows = 10
    table_handler = PeriodTable().attach_previous()
    period_table = table_handler.build_table(
        r_template = row_template,
        t_template = table_template,
        limit = nb_displayed_rows
    )
    
    forecaster = Forecaster(records = table_handler.period_starts())\
        .build_durations()\
        .forecast_date()
    template = render_template(
        'home.html',
        forecast = forecaster.format_forecast(forecaster.forecast),
        distance = forecaster.today_distance(),
        period_table = period_table
    )
    return template


@application.route('/', methods = ['POST'])
def add_date():
    try:
        dates_handler = PeriodDateHandler()
        dates_handler.set_dates(
            start_date = request.form["start_date"],
            end_date = request.form["end_date"],
            fmt = "%Y-%m-%d"
        )
    except ValueError:
        pass
    else:
        dates_handler.write_row()
    return home()


if __name__ == '__main__':
    application.run(host= '127.0.0.1', port=8000, debug=True)
