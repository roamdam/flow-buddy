
import unittest
from unittest.mock import patch
from datetime import date
from app.backend.forecaster import Forecaster


class Validation(unittest.TestCase):
    
    def test_assert_dates(self):
        records = [1, 2, 3]
        with self.assertRaises(TypeError):
            Forecaster._validate_records(records = records)
            
    def test_sort_dates(self):
        records = [date(2010, 1, 1), date(2010, 1, 3), date(2010, 1, 2)]
        response = Forecaster._validate_records(records = records)
        target = [date(2010, 1, 3), date(2010, 1, 2), date(2010, 1, 1)]
        
        self.assertEqual(target, response)


class BuildDuration(unittest.TestCase):
    
    @patch('app.backend.forecaster.Forecaster._validate_records')
    def test_durations(self, mvalidate):
        records = [date(2010, 1, 1), date(2010, 1, 3), date(2010, 1, 2)]
        mvalidate.return_value = records
        target = [-2, 1]
        
        poulpe = Forecaster(records = records).build_durations()
        
        self.assertEqual(target, poulpe.durations)


class ForecastDate(unittest.TestCase):
    
    def test_date_from_durations(self):
        poulpe = Forecaster(records = [date(2010, 1, 1)])
        poulpe.durations = [2, 2, 4, 4]
        poulpe.forecast_date()
        self.assertTrue(isinstance(poulpe.forecast, date))

    @patch('app.backend.forecaster.date')
    def test_today_delay(self, mock_date):
        poulpe = Forecaster(records = [])
        poulpe.forecast = date(2010, 1, 5)
        mock_date.today.return_value = date(2010, 1, 1)
        
        response = poulpe.today_distance()
        self.assertEqual(4, response)
        
    def test_format_forecast_one(self):
        response = Forecaster.format_forecast(x = date(2010, 1, 1))
        self.assertEqual("Friday 1 January", response)
        
    def test_format_forecast_two(self):
        response = Forecaster.format_forecast(x = date(2010, 1, 13))
        self.assertEqual("Wednesday 13 January", response)
