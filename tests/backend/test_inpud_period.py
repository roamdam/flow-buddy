
import unittest
from unittest.mock import patch

from datetime import date
from datetime import datetime
from app.backend.input_period import PeriodDateHandler


class TestDateConversion(unittest.TestCase):
    
    def test_not_a_date(self):
        empty_date = ""
        with self.assertRaises(ValueError):
            PeriodDateHandler.to_date(empty_date, fmt = "%Y-%m-%d", tolerance = False)
        
    def test_tolerance(self):
        input_date = "2019/04/05"
        response = PeriodDateHandler.to_date(input_date, fmt = "%Y-%m-%d", tolerance = True)
        self.assertIsNone(response)
            
    def test_input_script(self):
        input_date = "<script></script>"
        response = PeriodDateHandler.to_date(input_date, fmt = "%Y-%m-%d", tolerance = True)
        self.assertIsNone(response)
        
    def test_ok_input(self):
        input_date = "2019-12-31"
        target = datetime(2019, 12, 31, 0, 0)
        response = PeriodDateHandler.to_date(input_date, fmt = "%Y-%m-%d", tolerance = False)
        self.assertEqual(target, response)
        
        
class TestSetDates(unittest.TestCase):
    
    @patch('app.backend.input_period.PeriodDateHandler.to_date')
    def test_none_tolerance(self, mtodate):
        mtodate.return_value = None
        handler = PeriodDateHandler().set_dates(start_date = "", end_date = "", fmt = "%Y")
        self.assertIsNone(handler.start)
        self.assertIsNone(handler.end)
        
    @patch('app.backend.input_period.PeriodDateHandler.to_date')
    def test_same_date(self, mtodate):
        mtodate.return_value = date(2019, 1, 1)
        with self.assertRaises(ValueError):
            PeriodDateHandler().set_dates(start_date = "2019-01-01", end_date = "", fmt = "%Y-%m-%d")
        
    def test_ok_dates(self):
        handler = PeriodDateHandler().set_dates(start_date = "2019-01-01", end_date = "2019-01-04", fmt = "%Y-%m-%d")
        
        self.assertEqual(handler.start, datetime(2019, 1, 1, 0, 0))
        self.assertEqual(handler.end, datetime(2019, 1, 4, 0, 0))
