
import unittest
from unittest.mock import patch

from datetime import date
from datetime import datetime
from app.backend.period_table import PeriodTable


class TestStaticMethods(unittest.TestCase):
    
    def test_format_date(self):
        response = PeriodTable.format_date(datetime(2019, 1, 1, 0, 0, 0, 0), target_format = "%Y-%m-%d")
        self.assertEqual("2019-01-01", response)

    def test_format_str_error(self):
        with self.assertRaises(AttributeError):
            PeriodTable.format_date("2019-01-01", target_format = "%Y-%m-%d")

    def test_duration(self):
        x = date(2019, 1, 1)
        y = date(2019, 1, 2)
        response = PeriodTable.duration(x, y)
        
        self.assertEqual(1, response)
        
    def test_x_assertion(self):
        x = "2019-01-01"
        y = date(2019, 1, 2)
        with self.assertRaises(AssertionError):
            PeriodTable.duration(x, y)
    
    def test_y_assertion(self):
        x = date(2019, 1, 2)
        y = "2019-01-01"
        with self.assertRaises(AssertionError):
            PeriodTable.duration(x, y)
     
     
class AttachPrevious(unittest.TestCase):
    
    @patch('app.backend.period_table.PeriodTable.get_table')
    def test_attach_normal_case(self, mget):
        mget.return_value = [(1, 2), (3, 4)]
        target = [
            {"start": 1, "end": 2, "previous": 3},
            {"start": 3, "end": 4, "previous": None}

        ]
        table = PeriodTable()
        table.attach_previous()
        
        self.assertEqual(target, table.rows)

    @patch('app.backend.period_table.PeriodTable.get_table')
    def test_attach_no_records(self, mget):
        mget.return_value = []
        target = []
        table = PeriodTable()
        table.attach_previous()
    
        self.assertEqual(target, table.rows)

    @patch('app.backend.period_table.PeriodTable.get_table')
    def test_attach_one_record(self, mget):
        mget.return_value = [(1, 2)]
        target = [{"start": 1, "end": 2, "previous": None}]
        table = PeriodTable()
        table.attach_previous()
    
        self.assertEqual(target, table.rows)
        
        
class BuildTable(unittest.TestCase):

    @patch('app.backend.period_table.PeriodTable.get_table')
    def test_build_nodict_assertion(self, mget):
        mget.return_value = [(1, None), (3, 4)]
        table = PeriodTable()
        with self.assertRaises(AssertionError):
            table.build_table(r_template = "", t_template = "", limit = 1)
            
    @patch('app.backend.period_table.PeriodTable.get_table')
    def test_none_end_tolerance(self, _):
        target = "a table a row 01 January (Tuesday) 31 ¯\\_(ツ)_/¯"
        table = PeriodTable()
        table.rows = [
            {"start": date(2019, 1, 1), "end": None, "previous": date(2018, 12, 1)}
        ]
        response = table.build_table(r_template = "a row %s %s %s", t_template = "a table %s", limit = 10)
    
        self.assertEqual(target, response)

    @patch('app.backend.period_table.PeriodTable.get_table')
    def test_none_previous_tolerance(self, _):
        target = "a table a row 01 January (Tuesday) ¯\\_(ツ)_/¯ 1"
        table = PeriodTable()
        table.rows = [
            {"start": date(2019, 1, 1), "end": date(2019, 1, 2), "previous": None}
        ]
        response = table.build_table(r_template = "a row %s %s %s", t_template = "a table %s", limit = 10)
    
        self.assertEqual(target, response)
    
    @patch('app.backend.period_table.PeriodTable.get_table')
    def test_build_table(self, _):
        target = "a table a row 01 January (Tuesday) 31 1"
        table = PeriodTable()
        table.rows = [
            {"start": date(2019, 1, 1), "end": date(2019, 1, 2), "previous": date(2018, 12, 1)}
        ]
        response = table.build_table(r_template = "a row %s %s %s", t_template = "a table %s", limit = 2)

        self.assertEqual(target, response)
        

class RecordsSimple(unittest.TestCase):
    
    @patch('app.backend.period_table.PeriodTable.get_table')
    def test_yield_records(self, mget):
        mget.return_value = [{"start": 1}, {"start": 4}]
        response = PeriodTable().period_starts()
        target = [1, 4]
        
        self.assertEqual(target, response)
